<?php

namespace app\source\inflaters;
use app\models\employments\CarRentEmployment;
use yii\base\Exception;

/**
 * Class TagsInflater
 * @package app\source\inflaters
 * Генерирует содержания договора на основании какой-либо модели
 *
 */
class TagsInflater implements IInflater
{
    /** @var $carRentEmployment CarRentEmployment */
    private $carRentEmployment;
    /** $contractHTML string */
    private $contractHTML;
    /** $tags array */
    private $tags;
    /** $extraTags array */
    private $extraTags;

    function __construct($carRentEmployment, $contractHTML, $extraTags = [])
    {
        if(($carRentEmployment instanceof IInflatable) === false)
            throw new Exception('$carRentEmployment должен быть унаследован от интерфейса IInflatable');


        $this->carRentEmployment = $carRentEmployment;
        $this->tags = $this->carRentEmployment->tags();
        $this->contractHTML = $contractHTML;
        $this->extraTags = $extraTags;
    }

    /**
     * Генерирует договор
     * @inheritdoc
     * return string
     */
    public function inflate()
    {
        $result = $this->contractHTML;

        foreach ($this->tags as $name => $value){
            $result = str_replace($name, $value, $result);
        }

        foreach ($this->extraTags as $name => $value){
            $result = str_replace($name, $value, $result);
        }

        return $result;
    }
}