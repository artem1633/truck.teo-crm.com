<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 14.12.2017
 * Time: 1:45
 */

namespace app\source\counters;

/**
 * Class DilersDebitsComputer
 * @package app\source\counters
 * Считает сумму дохода поставщика от авто из всех броней
 *
 * @property \app\models\Cars $car
 */
class DilersDebitsComputer implements IComputer
{
    public $car;

    /**
     * DilersDebitsComputer constructor.
     */
    function __construct($car)
    {
        $this->car = $car;
    }

    /**
     * Считает доход
     * @inheritdoc
     */
    public function compute()
    {
        $debit = 0;
        $rentEmployments = $this->car->carRentEmployments;
        $diler = $this->car->diler;

        foreach ($rentEmployments as $employment)
        {
            // TODO: Поставить правильный подсчет
            $debit += $diler != null ? ($employment->rent_price_paid/100)*$diler->percent_of_profit : 0;
        }

        return $debit;
    }
}