<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;
use app\models\Settings;
use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        if($request->isPost)
        {
            $data = $request->post();
            foreach ($data['Settings'] as $key => $value){
                $setting = Settings::findByKey($key);

                if($setting != null){
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }
        }

        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if(Yii::$app->user->identity->role != User::ROLE_ADMIN)
            throw new ForbiddenHttpException('Доступ запрещен');

        return parent::beforeAction($action);
    }
}
