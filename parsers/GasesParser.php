<?php

namespace app\parsers;

use app\models\Devices;
use yii\base\BaseObject;

/**
 * Class GasesParser
 * @package app\parsers
 */
class GasesParser extends BaseObject
{
    public $ip;

    private function getData()
    {
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$this->ip);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function getParams()
    {
        $params = [];
        $data = $this->getData();

        if($data != null)
        {
            $dom = new \DOMDocument;
            $dom->loadHTML($data);
            /** @var \DOMElement[] $rows */
            $rows = $dom->getElementsByTagName('tr');

            foreach ($rows as $row) {
                /** @var \DOMElement[] $tds */
                $tds = $row->getElementsByTagName('td');
                if(count($tds) == 1)
                {
                    $params[trim($tds[0]->nodeValue)] = trim($tds[0]->nodeValue);
                }
            }
        }

        return $params;
    }

    public function getGases()
    {
        $gases = [];
        $data = $this->getData();

        if($data != null)
        {
            $dom = new \DOMDocument;
            $dom->loadHTML($data);
            /** @var \DOMElement[] $rows */
            $rows = $dom->getElementsByTagName('tr');

            foreach ($rows as $row) {
                /** @var \DOMElement[] $tds */
                $tds = $row->getElementsByTagName('td');
                if(count($tds) == 1)
                {
                    $gases[trim($tds[0]->nodeValue)] = trim($tds[1]->nodeValue);
                }
            }
        }

        return $gases;
    }
}