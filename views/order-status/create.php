<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrderStatus */

?>
<div class="order-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
