<?php


?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Монитор</h4>
    </div>
    <div class="panel-body">

        <?= $this->render('_search', ['model' => $searchModel]) ?>

        <?php if($model != null && $gasesArray != null){ ?>

                <?= \app\widgets\Chart\Chart::widget([
                    'id' => "chart",
                    'data' => [
                        'y' => $gasesArray,
                        'x' => $createdAtArray
                    ],
                ]) ?>

        <?php } else { ?>
            <hr>
            <p class="text-center">Данных не найдено</p>
            <hr>
        <?php } ?>
    </div>
</div>