<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Cars */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */
?>

<div class="cars-form">

    <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => ["{$action}"]]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'mark')->textInput(['maxlength' => true]) ?>

    <div class="row" style="display: flex; align-items: center;">
        <div class="col-md-10" style="padding:0 6px ;">
            <?= $form->field($model, 'voditel_id')->widget(Select2::class, [
                'data' => $data,
                'options' => ['multiple' => false],
            ]) ?>
        </div>
        <div class="col-md-2" style="padding: 0">
            <?= Html::a("<i class='fa fa-plus'></i>", ['cars/create-driver'], [
                'class' => 'btn btn-default btn-block',
                'style' => 'margin-top: 10px;',
                'role' => 'modal-remote',
                'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "cars/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
            ]) ?>

        </div>
    </div>

    <?= $form->field($model, 'tracker')->textInput(['maxlength' => true]) ?>    
    
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
