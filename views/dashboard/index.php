<?php

use yii\helpers\ArrayHelper;

$this->title = 'Рабочий стол';

$devices = ArrayHelper::map(\app\models\Devices::find()->where(['is_in_dashboard' => 1])->all(), 'id', 'name');

?>

<!-- begin row -->
<div class="row">
    <!-- begin col-3 -->
    <div class="col-md-4 col-sm-4">
        <div class="widget widget-stats bg-green">
            <div class="stats-icon"><i class="fa fa-bolt"></i></div>
            <div class="stats-info">
                <h4>ВСЕГО УСТРОЙСТВ</h4>
                <p><?= \app\models\Devices::find()->count() ?></p>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-4 col-sm-4">
        <div class="widget widget-stats bg-blue">
            <div class="stats-icon"><i class="fa fa-area-chart"></i></div>
            <div class="stats-info">
                <h4>ВСЕГО ДАННЫХ</h4>
                <p><?= \app\models\GasesValues::find()->count() ?></p>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-md-4 col-sm-4">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
            <div class="stats-info">
                <h4>ПОСЛЕДНЯЯ ОЧИСТКА БД</h4>
                <?php if(($log = \app\models\Logs::find()->where(['event' => \app\models\Logs::LOG_DB_TRUNCATED])->orderBy('created_at desc')->one()) != null){ ?>
                    <p><?= Yii::$app->formatter->asDate($log->created_at, 'php:Y-m-d H:i:s') ?></p>
                <?php } else { ?>
                    <p>---</p>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- end col-3 -->
</div>
<!-- end row -->

<?php if($devices != null): ?>

    <?php foreach ($devices as $id => $name):
            $data = \app\models\Devices::getStatistics($id);
        ?>

        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"><?= $name ?></h4>
            </div>
            <div class="panel-body">
                <?= \app\widgets\Chart\Chart::widget([
                    'id' => 'chart-'.$id,
                    'data' => [
                        'y' => $data['gasesArray'],
                        'x' => $data['createdAtArray']
                    ],
                ]) ?>
            </div>
        </div>

    <?php endforeach; ?>


<?php endif; ?>
