<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrackerSettings */

?>
<div class="tracker-settings-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
