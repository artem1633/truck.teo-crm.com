<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GasesValuesSearch */

?>

<?php $form = ActiveForm::begin(['method' => 'get', 'id' => 'search-form']); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($searchModel, 'device_id')->widget(kartik\select2\Select2::class, [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Devices::find()->all(), 'id', 'name'),
                'options' => ['multiple' => true],
            ])->label('ip') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($searchModel, 'gases')->widget(kartik\select2\Select2::class, [
                'options' => ['multiple' => true],
            ])->label('Газы') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($searchModel, 'created_at_range')->widget(kartik\daterange\DateRangePicker::class, [
                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'Y-m-d',
                    ],
                ],
            ])->label('Дата') ?>
        </div>
    </div>


<div class="form-group">
    <?= Html::submitButton('Готово', ['class' => 'btn btn-success', 'onclick' => '
              $("#search-form").attr("action", "'.\yii\helpers\Url::toRoute(['index']).'").submit();
    ']) ?>
    <?= Html::tag('div','<i class="fa fa-file-excel-o"></i> Экспорт',['class' => 'btn btn-primary', 'onclick' => '
              $("#search-form").attr("action", "'.\yii\helpers\Url::toRoute(['csv-export']).'").submit();
    ']) ?>
</div>

<?php ActiveForm::end() ?>

<?php

$script = <<< JS
    $('#gasesvaluessearch-device_id').change(function()
    {
       var val = $(this).val();
       
       console.log(val.join(','));
       
       $.get('/devices/get-gases?id='+val.join(','), function(response){
             $('#gasesvaluessearch-gases').html('').trigger('change'); 
             $.each(response, function(i, key){
                  $('#gasesvaluessearch-gases').append(new Option(i, key, false, false)).trigger('change');
             });
       });
       
    });

    $('#gasesvaluessearch-device_id').trigger('change');

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
