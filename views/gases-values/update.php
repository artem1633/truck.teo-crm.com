<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GasesValues */
?>
<div class="gases-values-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
