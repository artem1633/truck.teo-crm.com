<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Templates */

?>
<div class="templates-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
