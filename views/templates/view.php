<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */
?>
<div class="templates-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text:ntext',
        ],
    ]) ?>

</div>
