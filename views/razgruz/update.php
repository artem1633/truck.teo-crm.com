<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Razgruz */
?>
<div class="razgruz-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
