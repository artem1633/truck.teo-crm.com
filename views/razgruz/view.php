<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Razgruz */
?>
<div class="razgruz-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
