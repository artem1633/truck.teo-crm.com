<?php
/* @var $this yii\web\View */
$this->title = 'Трэкер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Трэкер</h4>
    </div>
    <div class="panel-body">

        <?= $this->render('_search', ['model' => $searchModel]) ?>

        
    </div>
</div>