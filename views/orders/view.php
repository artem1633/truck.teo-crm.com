<?php

use app\models\OrderStatus;
use app\models\Razgruz;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="orders-view">

<ul class="nav nav-tabs bg-dark">
  <li class="active"><a data-toggle="tab" href="#home">Информация</a></li>
  <li><a data-toggle="tab" href="#menu2">Комментарии</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">          
           <div class="row">
               <div class="col-md-6">
                   <?= DetailView::widget([
                       'model' => $model,
                       'attributes' => [
                           'id',
                           [
                               'label'=>'ID Клиента',
                               'value'=> $model->client_id . " (" . $model->client_idText . ")" ,
                           ],
                           'client_contact',
                           'razgruz_address',
                           [
                               'label'=>'ID Автомобиля',
                               'value'=> $model->car_id . " (" . $model->car_idText . ")" ,
                           ],
                           'car_mark',
                           'car_number',
                           [
                               'attribute'=>'voditel_id',
                               'value' => function($model){
                                   $driver = ArrayHelper::getValue(User::find()->where(['id' => $model->voditel_id])->one(),'name');
                                   return $driver;
                               }
                           ],
                           'driver_zp',
                           [
                               'attribute'=>'status_id',
                               'value' => function($model){
                                   $name = ArrayHelper::getValue(OrderStatus::find()->where(['id' => $model->status_id])->one(), 'name');
                                   return $name;
                               },
                           ],

                       ],
                   ]) ?>
               </div>
               <div class="col-md-6">
                   <?= DetailView::widget([
                       'model' => $model,
                       'attributes' => [
                           'container_number',
                           'container_type',
                           'container_removed_num',
                           'container_put_num',
                           [
                               'attribute'=>'pay_type',
                               'value' => function ($model) {
                                   if ($model->pay_type === 0){
                                       return 'Наличные';
                                   }elseif ($model->pay_type === 1){
                                       return 'Безналичные';
                                   }else{
                                       return 'Не задано';
                                   }
                               },
                           ],
                           'price',
                           [
                               'attribute'=>'razgruz_id',
                               'value' => function($model){
                                   $name = ArrayHelper::getValue(Razgruz::find()->where(['id' => $model->razgruz_id])->one(), 'name');
                                   return $name;
                               },
                           ],
                           'export_date',
                           'time' ,
                           'contract_link',
                       ],
                   ]) ?>
               </div>
               <div class="col-md-12">
                   <div class="panel panel-inverse">
                       <div class="panel-heading">
                           <h4 class="panel-title">Примечания</h4>
                           <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning"  data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                           </div>
                       </div>
                       <div class="panel-body" style="height: 100px; overflow-y: auto;background-color: #f0f3f5">
                           <?= $model->comment?>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>

  </div>
  <div id="menu1" class="tab-pane fade">
    
  </div>
  <div id="menu2" class="tab-pane fade">
    <?php echo \yii2mod\comments\widgets\Comment::widget([
        'model' => $model,
    ]); ?>   
  </div>
</div>



</div>
