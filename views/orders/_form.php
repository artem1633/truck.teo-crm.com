<?php

use app\models\OrderStatus;
use app\models\Razgruz;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\time\TimePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

CrudAsset::register($this);
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => ["orders/{$action}"]]); ?>
        
    <div class="row">
        <div class="col-md-4">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10" style="padding:0 6px ;">
                    <?= $form->field($model, 'client_id')->widget(Select2::class, [
                        'data' => $dataToSelect['clients'],
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите клиента'],
                    ]) ?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['orders/create-clients'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "orders/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'client_contact')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'razgruz_address')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10" style="padding:0 6px ;">
                    <?= $form->field($model, 'car_id')->widget(Select2::class, [
                        'data' => $dataToSelect['cars'],
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите автомобиль'],
                    ]) ?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['orders/create-cars'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "orders/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'car_mark')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'car_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'voditel_id')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'container_number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'container_type')->dropDownList([
                'Замена' => 'Замена',
                'Установка' => 'Установка',
                'Аткат' => 'Аткат',
                'Снял' => 'Снял'
            ]) ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'container_removed_num')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'container_put_num')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'pay_type')->dropDownList([
                0 => 'Наличные',
                1 => 'Безналичные'
            ]) ?>

        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'driver_zp')->textInput(['maxlength' => true])?>
        </div>

        <div class="col-md-3">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10" style="padding:0 6px ;">
                    <?= $form->field($model, 'razgruz_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(Razgruz::find()->all(), 'id', 'name'),
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите адрес'],
                    ]) ?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['orders/create-razgruz'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "orders/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
    <div class="row">


        <div class="col-md-3">
            <?= $form->field($model, 'export_date')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?=  $form->field($model, 'time')->widget(TimePicker::classname(), []);?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(OrderStatus::find()->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите контрагента ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'contract_link')->textInput(['maxlength' => true]) ?>
        </div>


    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>    
    
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php
    
    $script = <<< JS
        $('.orders-form>form').on('afterValidateAttribute', function (event, attribute, message) {

            switch(attribute.name) {
                case 'car_id':
                    $.ajax({
                        url: "/orders/car/?id="+attribute.value,
                        type: "POST",
                        response: "json",                            
                        success: function(response) {
                            response = JSON.parse(response);
                            console.log(response["name"] + response["voditel_id"]);
                            $('#orders-car_mark').val(response["mark"]);
                            $('#orders-car_number').val(response["number"]);
                            $('#orders-voditel_id').val(response["voditel_id"]);                             
                        },
                        error: function(response) {
                            console.log('false');
                            return false;
                        }
                    });

                    break;

                case 'client_id':
                    $.ajax({
                        url: "/orders/client/?id="+attribute.value,
                        type: "POST",
                        response: "json",                            
                        success: function(response) {
                            response = JSON.parse(response);
                            $('#orders-client_contact').val(response["contact"]);
                            // $('#orders-client_address').val(response["address"]);
                            $('#orders-razgruz_address').val(response["razgruz_address"]);                             
                        },
                        error: function(response) {
                            console.log('false');
                            return false;
                        }
                    });

                    break;                  
            }

        });            

JS;

    $this->registerJs($script, \yii\web\View::POS_READY);
?>
