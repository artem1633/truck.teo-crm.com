<?php

use app\models\Cars;
use app\models\OrderStatus;
use app\models\Razgruz;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\User;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'export_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'value' => function ($model) {
            return $model->client_id . " (" . $model->client_idText . ")" ;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'razgruz_id',
        'content' => function($data){
            $name = ArrayHelper::getValue(Razgruz::find()->where(['id' => $data->razgruz_id])->one(), 'name');
            return $name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pay_type',
        'value' => function ($model) {
            if ($model->pay_type === 0){
                return 'Наличные';
            }elseif ($model->pay_type === 1){
                return 'Безналичные';
            }else{
                return 'Не задано';
            }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'create_date',
//    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_mark',        
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_number',
        'label' => 'Номер а/м'
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'container_number',
        'label' => 'Объем к-ра'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'container_type',
        'label' => 'Тип работ'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'container_removed_num',
        'label' => 'Поставил'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'container_put_num',
        'label' => 'Снял'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'voditel_id',
        'value' => function($model){
            $driver = ArrayHelper::getValue(User::find()->where(['id' => $model->voditel_id])->one(),'name');
            return $driver;
        }
    ],


     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'driver_zp',
     ],

     [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_id',
        'value' => function($model){
            $name = ArrayHelper::getValue(OrderStatus::find()->where(['id' => $model->status_id])->one(), 'name');
            return $name;
        },                      
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => Yii::$app->user->identity->role != User::ROLE_VODITEL ? '{view}{update}{delete}{update-car-address}{view-on-map}' : '{view-voditel}{update-teh-status}{update-otgruz-status}{update-tonaj}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Просмотреть',
                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
//            'update-teh-status' => function ($url, $model) {
//                return Html::a('<i class="fa fa-cog text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить тех. состояние',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
//            'update-otgruz-status' => function ($url, $model) {
//                return Html::a('<i class="fa fa-truck text-success" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить статус отгрузки',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
//            'update-tonaj' => function ($url, $model) {
//                return Html::a('<i class="fa fa-database text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить тоннаж',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
//            'update-car-address' => function ($url, $model) {
//                return Html::a('<i class="fa fa-map-pin text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Обновить местоположение',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
//            'view-voditel' => function ($url, $model) {
//                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Просмотреть',
//                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
//            'view-on-map' => function ($url, $model) {
//                return Html::a('<i class="fa fa-map text-info" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Просмотреть авто на карте',
//                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            },
            
        ], 
    ],

];   