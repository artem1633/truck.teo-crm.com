<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razgruz".
 *
 * @property int $id
 * @property string $name Адрес разгрузки
 *
 * @property Orders[] $orders
 */
class Razgruz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razgruz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Адрес разгрузки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['razgruz_id' => 'id']);
    }


    /**
     * {@inheritdoc}
     * @return RazgruzQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RazgruzQuery(get_called_class());
    }
}
