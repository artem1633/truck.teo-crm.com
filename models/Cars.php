<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property string $name
 * @property string $number
 * @property string $tracker
 * @property string $mark
 * @property int $voditel_id
 *
 * @property User $voditel
 */
class Cars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'number', 'voditel_id'], 'required'],
            [['voditel_id'], 'integer'],
            [['name', 'number', 'mark', 'status', 'tracker'], 'string', 'max' => 255],
            [['voditel_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['voditel_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'number' => 'Номер',
            'mark' => 'Марка',
            'voditel_id' => 'ID Водителя',
            'status' => 'Статус',
            'tracker' => 'Трэкер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoditel()
    {
        return $this->hasOne(User::className(), ['id' => 'voditel_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status = "Свободен"; 
            }
            return true;
        }
        return false;
    }

    public function getVoditel_idText()
    {
        return $this->voditel->name;
    }
    
    public function setVoditel_idText($value)
    {
        $this->voditel_id = $this->voditel_id;
    }

}
