<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'car_id', 'voditel_id', 'tonaj_id', 'in_city'], 'integer'],
            [['create_date', 'price', 'razgruz_address', 'car_number',
                'razgruz_id', 'tech_status', 'otgruz_status','car_address',
                'car_mark', 'razgruz_date',  'client_contact', 'pay_type', 'container_number', 'container_type', 'container_removed_num',
                'container_put_num', 'contract_link', 'comment', 'time','status_id'],
                'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'car_id' => $this->car_id,
            'voditel_id' => $this->voditel_id,
            'tonaj_id' => $this->tonaj_id,
            'in_city' => $this->in_city,
            'pay_type' => $this->pay_type,
            'status_id' => $this->status_id,
            'razgruz_id' => $this->razgruz_id

        ]);

        $query->andFilterWhere(['like', 'create_date', $this->create_date])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'razgruz_address', $this->razgruz_address])
            ->andFilterWhere(['like', 'car_number', $this->car_number])
            ->andFilterWhere(['like', 'tech_status', $this->tech_status])
            ->andFilterWhere(['like', 'otgruz_status', $this->otgruz_status])
            ->andFilterWhere(['like', 'car_address', $this->car_address])
            ->andFilterWhere(['like', 'car_mark', $this->car_mark])
            ->andFilterWhere(['like', 'client_contact', $this->client_contact])
            ->andFilterWhere(['like', 'container_number', $this->container_number])
            ->andFilterWhere(['like', 'container_type', $this->container_type])
            ->andFilterWhere(['like', 'container_removed_num', $this->container_removed_num])
            ->andFilterWhere(['like', 'container_put_num', $this->container_put_num])
            ->andFilterWhere(['like', 'contract_link', $this->contract_link])
//            ->andFilterWhere(['like', 'for_load', $this->for_load])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'razgruz_date', $this->razgruz_date]);

        return $dataProvider;
    }
}
