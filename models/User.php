<?php

namespace app\models;

use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $password_hash
 * @property string $password
 * @property integer $status
 * @property string $email
 * @property integer $is_deletable
 * @property string $phone
 *
 * @property UserDevices[] $userDevices
 * @property Devices[] $devices
 */
class User extends \app\base\AppActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 'ADMIN';
    const ROLE_DIRECTOR = 'DIRECTOR';
    const ROLE_ZAMDIRECTOR = 'ZAMDIRECTOR';
    const ROLE_MENLOGISTIC = 'MENLOGISTIC';
    const ROLE_MENPRODAJ = 'MENPRODAJ';
    const ROLE_BUHTRANSOTD = 'BUHTRANSOTD';
    const ROLE_MEHANIK = 'MEHANIK';
    const ROLE_VODITEL = 'VODITEL';    

    public $password;

    private $oldPasswordHash;

    public $accessDevices;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'пользователь';
    }

    /**
     * @inheritdoc
     */
    function getViewUrl()
    {
        return Url::toRoute(['user/view', 'id' => $this->id]);
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'access_devices', 'login', 'role', 'is_deletable', 'password', 'password_hash'],
            self::SCENARIO_EDIT => ['name', 'access_devices', 'login', 'role', 'is_deletable', 'password', 'password_hash'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'role',], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
            ['login', 'match', 'pattern' => '/^[a-z]+([-_]?[a-z0-9]+){0,2}$/i', 'message' => '{attribute} должен состоять только из латинских букв и цифр'],
            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            [['is_deletable'], 'integer'],
            [['login', 'role', 'password_hash', 'password', 'name'], 'string', 'max' => 255],

            [['accessDevices'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        UserDevices::deleteAll(['user_id' => $this->id]);

        if(Yii::$app->request->post()['User']['accessDevices'] != null)
        {
            foreach (Yii::$app->request->post()['User']['accessDevices'] as $deviceId)
            {
                (new UserDevices(['user_id' => $this->id, 'devices_id' => $deviceId]))->save();
            }
        }

        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'role' => 'Роль',
            'password_hash' => 'Password Hash',
            'password' => 'Пароль',
            'status' => 'Статус',
            'accessDevices' => 'Доступные устройства',
            'email' => 'Email',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
        ];
    }

    /**
     * Возвращает список ролей
     * @return array
     */
    public static function getRolesLabels()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_DIRECTOR => 'Директор',
            self::ROLE_ZAMDIRECTOR => 'Зам. Директор',
            self::ROLE_MENLOGISTIC => 'Менеджер по логистике',
            self::ROLE_MENPRODAJ => 'Менеджер по продажам',
            self::ROLE_BUHTRANSOTD => 'Бухгалтер транспортного отдела',
            self::ROLE_MEHANIK => 'Механик',
            self::ROLE_VODITEL => 'Водитель'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['id' => 'devices_id'])->viaTable('user_devices', ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getUsername()
    {
        return $this->name;
    }
}
