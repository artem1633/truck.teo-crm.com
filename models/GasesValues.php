<?php

namespace app\models;

use app\queries\GasesValuesQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "gases_values".
 *
 * @property int $id
 * @property int $device_id Устройство
 * @property string $key Наименование значения
 * @property double $value Значение
 * @property string $created_at
 *
 * @property Devices $device
 */
class GasesValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gases_values';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new GasesValuesQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_id'], 'integer'],
            [['key'], 'string', 'max' => 255],
            [['value'], 'number'],
            [['created_at'], 'safe'],
            [['device_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devices::className(), 'targetAttribute' => ['device_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'Device ID',
            'key' => 'Параметр',
            'value' => 'Значение',
            'created_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Devices::className(), ['id' => 'device_id']);
    }
}
