<?php

use yii\db\Migration;

/**
 * Handles the creation of table `export_autosave`.
 */
class m180616_185623_create_export_autosave_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('export_autosave', [
            'id' => $this->primaryKey(),
            'device_id' => $this->string()->comment('ID устройств'),
            'gases' => $this->string()->comment('Газы'),
            'date_start' => $this->date()->comment('С'),
            'date_end' => $this->date()->comment('По'),
            'interval' => $this->integer()->comment('Интервал обновления'),
            'folder_name' => $this->string()->comment('Папка сохранения'),
            'last_export_datetime' => $this->dateTime()->comment('Последние время экспорта'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('export_autosave');
    }
}
