<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m180624_200036_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'razgruz_address' => $this->string()->notNull(),
            'contact' => $this->string()->notNull()            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clients');
    }
}
