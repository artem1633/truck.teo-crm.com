<?php

use yii\db\Migration;

/**
 * Class m210103_023157_add_order_status_column
 */
class m210103_023157_add_order_status_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order_status', 'sms', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order_status', 'sms');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210103_023157_add_order_status_column cannot be reverted.\n";

        return false;
    }
    */
}
