<?php

use yii\db\Migration;

/**
 * Class m201229_120917_add_status_to_orders
 */
class m201229_120917_add_status_to_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'status_id', $this->integer());
        $this->createIndex(
            'idx-orders-status_id',
            'orders',
            'status_id'
        );

        $this->addForeignKey(
            'fk-orders-status_id',
            'orders',
            'status_id',
            'order_status',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-orders-status_id',
            'orders'
        );

        $this->dropIndex(
            'idx-orders-status_id',
            'orders'
        );
        $this->dropColumn('orders', 'status_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201229_120917_add_status_to_orders cannot be reverted.\n";

        return false;
    }
    */
}
