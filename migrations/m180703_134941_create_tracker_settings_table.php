<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tracker_settings`.
 */
class m180703_134941_create_tracker_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tracker_settings', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull(),
            'pass' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tracker_settings');
    }
}
