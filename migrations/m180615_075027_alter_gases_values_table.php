<?php

use yii\db\Migration;

/**
 * Class m180616_075027_alter_gases_values_table
 */
class m180615_075027_alter_gases_values_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('gases_values', 'CO');
        $this->dropColumn('gases_values', 'NO');
        $this->dropColumn('gases_values', 'NO2');
        $this->dropColumn('gases_values', 'SO2');

        $this->addColumn('gases_values', 'key', $this->string()->comment('Наименование параметра'));
        $this->addColumn('gases_values', 'value', $this->float()->comment('Значение параметра'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
