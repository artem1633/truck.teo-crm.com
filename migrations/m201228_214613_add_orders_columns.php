<?php

use yii\db\Migration;

/**
 * Class m201228_214613_add_orders_columns
 */
class m201228_214613_add_orders_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'client_contact', $this->string());
        $this->addColumn('orders', 'pay_type', $this->integer());
        $this->addColumn('orders', 'container_number', $this->string());
        $this->addColumn('orders', 'container_type', $this->string());
        $this->addColumn('orders', 'container_removed_num', $this->string());
        $this->addColumn('orders', 'container_put_num', $this->string());
        $this->addColumn('orders', 'contract_link', $this->string());
        $this->addColumn('orders', 'comment', $this->text());
        $this->addColumn('orders', 'for_load', $this->string());
        $this->addColumn('orders', 'time', $this->time());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'client_contact');
        $this->dropColumn('orders', 'pay_type');
        $this->dropColumn('orders', 'container_number');
        $this->dropColumn('orders', 'container_type');
        $this->dropColumn('orders', 'container_removed_num');
        $this->dropColumn('orders', 'container_put_num');
        $this->dropColumn('orders', 'contract_link');
        $this->dropColumn('orders', 'comment');
        $this->dropColumn('orders', 'for_load');
        $this->dropColumn('orders', 'time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201228_214613_add_orders_columns cannot be reverted.\n";

        return false;
    }
    */
}
