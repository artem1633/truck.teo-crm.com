<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%razgruz}}`.
 */
class m201230_141808_create_razgruz_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%razgruz}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Адрес разгрузки')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%razgruz}}');
    }
}
