<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cars`.
 */
class m180621_213548_create_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cars', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'number' => $this->string()->notNull(),
            'tracker' => $this->string(),
            'mark' => $this->string(),
            'voditel_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(
            'cars_user_id',
            'cars',
            'voditel_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cars');
    }
}
