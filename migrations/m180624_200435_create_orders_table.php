<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180624_200435_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),

            'create_date' => $this->string()->notNull(),

            'client_id' => $this->integer(),
            'client_address' => $this->string(),
            'razgruz_address' => $this->string(),

            'car_id' => $this->integer(),
            'car_number' => $this->string(),
            'voditel_id' => $this->integer(),

            'tonaj_id' => $this->integer(),

            'in_city' => $this->integer(),
            'pogruz_address' => $this->string(),
            'tech_status' => $this->string(),
            'otgruz_status' => $this->string(),
             
        ]);

        $this->addForeignKey(
            'orders_clients_id',
            'orders',
            'client_id',
            'clients',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'orders_cars_id',
            'orders',
            'car_id',
            'cars',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'orders_tonaj_id',
            'orders',
            'tonaj_id',
            'tonaj',
            'id',
            'CASCADE'
        );

        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }
}
