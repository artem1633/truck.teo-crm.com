<?php

use yii\db\Migration;

/**
 * Handles adding tracker to table `cars`.
 */
class m180703_143803_add_tracker_column_to_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cars', 'tracker', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cars', 'tracker');
    }
}
