<?php

use yii\db\Migration;

/**
 * Handles dropping tracker_login from table `cars`.
 */
class m180703_143637_drop_tracker_login_column_from_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('cars', 'tracker_login');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('cars', 'tracker_login', $this->string());
    }
}
