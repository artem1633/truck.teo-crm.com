<?php

use yii\db\Migration;

/**
 * Class m201230_123748_add_columns2_to_orders
 */
class m201230_123748_add_columns2_to_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'export_date', $this->date());
        $this->addColumn('orders', 'driver_zp', $this->float());

        $this->dropColumn('orders', 'client_address');

        $this->addColumn('orders', 'price', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'export_date');
        $this->dropColumn('orders', 'driver_zp');
        $this->dropColumn('orders', 'price');

        $this->addColumn('orders', 'client_address', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201230_123748_add_columns2_to_orders cannot be reverted.\n";

        return false;
    }
    */
}
