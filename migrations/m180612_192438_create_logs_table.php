<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs`.
 */
class m180612_192438_create_logs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('logs', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'event' => $this->string()->comment('Наименование события'),
            'text' => $this->text()->comment('Текст события'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-logs-user_id',
            'logs',
            'user_id'
        );

        $this->addForeignKey(
            'fk-logs-user_id',
            'logs',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-logs-user_id',
            'logs'
        );

        $this->dropIndex(
            'idx-logs-user_id',
            'logs'
        );

        $this->dropTable('logs');
    }
}
