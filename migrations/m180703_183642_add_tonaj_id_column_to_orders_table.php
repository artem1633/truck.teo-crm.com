<?php

use yii\db\Migration;

/**
 * Handles adding tonaj_id to table `orders`.
 */
class m180703_183642_add_tonaj_id_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'tonaj_id', $this->integer());
        
        $this->addForeignKey(
            'orders_tonaj_id',
            'orders',
            'tonaj_id',
            'tonaj',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'tonaj_id');
        
    }
}
