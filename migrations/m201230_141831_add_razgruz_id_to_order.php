<?php

use yii\db\Migration;

/**
 * Class m201230_141831_add_razgruz_id_to_order
 */
class m201230_141831_add_razgruz_id_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('orders', 'pogruz_address');
        $this->addColumn('orders', 'razgruz_id', $this->integer());
        $this->createIndex(
            'idx-orders-razgruz_id',
            'orders',
            'razgruz_id'
        );

        $this->addForeignKey(
            'fk-orders-razgruz_id',
            'orders',
            'razgruz_id',
            'razgruz',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('orders', 'pogruz_address', $this->string());

        $this->dropForeignKey(
            'fk-orders-razgruz_id',
            'orders'
        );

        $this->dropIndex(
            'idx-orders-razgruz_id',
            'orders'
        );
        $this->dropColumn('orders', 'razgruz_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201230_141831_add_razgruz_id_to_order cannot be reverted.\n";

        return false;
    }
    */
}
