<?php

use yii\db\Migration;

/**
 * Handles dropping tonaj_id from table `orders`.
 */
class m180703_183155_drop_tonaj_id_column_from_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey(
            'orders_tonaj_id',
            'orders'
        );

        $this->dropIndex(
            'orders_tonaj_id',
            'orders'
        );

        $this->dropColumn('orders', 'tonaj_id');  

        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('orders', 'tonaj_id', $this->integer());

        $this->addForeignKey(
            'orders_tonaj_id',
            'orders',
            'tonaj_id',
            'tonaj',
            'id',
            'CASCADE'
        );
    }
}
