<?php

use yii\db\Migration;

/**
 * Handles dropping tracker_pass from table `cars`.
 */
class m180703_143711_drop_tracker_pass_column_from_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('cars', 'tracker_pass');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('cars', 'tracker_pass', $this->stringng());
    }
}
