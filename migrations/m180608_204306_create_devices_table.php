<?php

use yii\db\Migration;

/**
 * Handles the creation of table `devices`.
 */
class m180608_204306_create_devices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('devices', [
            'id' => $this->primaryKey(),
            'ip' => $this->string()->comment('IP адрес'),
            'gases' => $this->string()->comment('Газы'),
            'comment' => $this->text()->comment('Комментарий'),
            'connection_time' => $this->string()->comment('Интервал обращения по ip'),
            'last_connection_datetime' => $this->dateTime()->comment('Предыдущие обращение'),
        ]);
        $this->addCommentOnTable('devices', 'Устройтсва');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('devices');
    }
}
