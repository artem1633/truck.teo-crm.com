<?php

use yii\db\Migration;

/**
 * Handles adding car_address to table `orders`.
 */
class m180629_170714_add_car_address_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'car_address', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'car_address');
    }
}
