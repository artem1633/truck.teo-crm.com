<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tonaj`.
 */
class m180621_220705_create_tonaj_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tonaj', [
            'id' => $this->primaryKey(),
            'value' => $this->float()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tonaj');
    }
}
