<?php

use yii\db\Migration;

/**
 * Handles adding razgruz_date to table `orders`.
 */
class m180706_154529_add_razgruz_date_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'razgruz_date', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'razgruz_date');
    }
}
