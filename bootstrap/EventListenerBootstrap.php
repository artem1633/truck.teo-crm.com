<?php

namespace app\bootstrap;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\helpers\Html;

use app\models\Logs;

/**
 * Class EventListenerBootstrap
 * @package app\bootstrap
 *
 * Слушает события приложения
 */
class EventListenerBootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;

        Event::on(\yii\web\User::class, \yii\web\User::EVENT_AFTER_LOGIN, function($data) use($user) {
            $log = new Logs([
                'user_id' => $data->identity->id,
                'event' => Logs::LOG_USER_AUTHORIZED,
                'text' => "Пользователь, '{$data->identity->login}' авторизовался"
            ]);
            $log->save();
        });

        Event::on(\app\base\AppActiveRecord::class, \app\base\AppActiveRecord::EVENT_AFTER_INSERT, function($data) use ($user) {
            $log = new Logs([
                'user_id' => $user != null ? $user->id : null,
                'event' => Logs::LOG_CREATED_RECORD,
                'text' => "Добавлена запись: ".Html::a($data->sender->getEntityName(), $data->sender->getViewUrl(), ['data-pjax' => 0, 'target' => '_blank']),
            ]);
            $log->save();
        });

        Event::on(\app\base\AppActiveRecord::class, \app\base\AppActiveRecord::EVENT_BEFORE_DELETE, function($data) use ($user) {
            $log = new Logs([
                'user_id' => $user != null ? $user->id : null,
                'event' => Logs::LOG_DELETED_RECORD,
                'text' => "Удалена запись: '".$data->sender->getEntityName()."' под ID '{$data->sender->id}'",
            ]);
            $log->save();
        });

        Event::on(\app\base\AppActiveRecord::class, \app\base\AppActiveRecord::EVENT_AFTER_UPDATE, function($data) use ($user) {
            $log = new Logs([
                'user_id' => $user != null ? $user->id : null,
                'event' => Logs::LOG_UPDATED_RECORD,
                'text' => "Обновлена запись: ".Html::a($data->sender->getEntityName(), $data->sender->getViewUrl(), ['data-pjax' => 0, 'target' => '_blank']),
            ]);
            $log->save();
        });

        Event::on(\app\controllers\GasesValuesController::class, \app\controllers\GasesValuesController::EVENT_CSV_EXPORTED, function($data) use ($user) {
            $log = new Logs([
                'user_id' => $user != null ? $user->id : null,
                'event' => Logs::LOG_EXPORTED_CSV,
                'text' => "Был совершен экспорт",
            ]);
            $log->save();
        });

        Event::on(\app\controllers\GasesValuesController::class, \app\controllers\GasesValuesController::EVENT_DB_TRUNCATED, function($data) use ($user) {
            $log = new Logs([
                'user_id' => $user != null ? $user->id : null,
                'event' => Logs::LOG_DB_TRUNCATED,
                'text' => "Была совершена очистка БД",
            ]);
            $log->save();
        });
    }
}