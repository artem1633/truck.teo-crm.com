<?php

namespace app\queries;

use app\models\Devices;
use yii\helpers\ArrayHelper;

/**
 * Class GasesValuesQuery
 * @see \app\models\Devices
 */
class GasesValuesQuery extends \yii\db\ActiveQuery
{

    /**
     * @var array
     */
    private $exportGases = [];

    public function allForCSVExport()
    {
        /** @var \app\models\GasesValues[] $models */
        $models = $this->all();
        $devices = ArrayHelper::map(Devices::find()->all(), 'id', 'id');

        $csvArray = [];

        foreach ($models as $model)
        {
            $csvArray[] = [
                $model->created_at,
                $devices[$model->device_id],
                $model->key,
                $model->value,
            ];
        }

        return $csvArray;
    }

    /**
     * @param array $gases
     */
    public function exportGases($gases)
    {
        $this->exportGases = $gases == null ? [] : $gases;
    }
}